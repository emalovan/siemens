import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by tom on 30/08/16.
 */
public class MatrixOpsTest {
    @Test
    public void rotateTest() throws Exception {
        char[][] initial  = {{1,2,3},
                             {4,5,6},
                             {7,8,9}};

        char[][] expected = {{3,6,9},
                             {2,5,8},
                             {1,4,7}};
        //Creating a new object of the class
        MatrixOps m = new MatrixOps(3,3);
        //Put the tested matrix to the object
        m.setMatrix(initial);
        //Rotate it and test after
        m.rotate();
        Assert.assertArrayEquals(expected, m.getMatrix());
    }

    @Test
    public void convertToSnailTest() throws Exception {
        char[][] initial  = {{'1','2','3'},
                             {'4','5','6'},
                             {'7','8','9'}};
        //Creating a new object of the class
        MatrixOps m2 = new MatrixOps(3,3);
        //Put the tested matrix to the object
        m2.setMatrix(initial);
        //Using convertion method for the object
        m2.convertToSnail();
        Assert.assertTrue("1,2,3,6,9,8,7,4,5".equals(m2.getSnail()));
    }

}