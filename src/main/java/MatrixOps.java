import java.util.Random;

/**
 * Created by tom on 30/08/16.
 */
public class MatrixOps {
    private char[][] matrix;
    private int x;
    private int y;
    private String snail = "";

    //Main constructor, initializing only matrix sizes
    public MatrixOps(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public char[][] getMatrix() {
        return matrix;
    }

    public void setMatrix(char[][] matrix) {
        this.matrix = matrix;
    }

    public String getSnail() {
        return snail;
    }

    //Generating random matrix with char values
    public void init() {
        this.matrix = new char[x][y];
        Random r = new Random();
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                matrix[i][j] = (char)(r.nextInt(26) + 'a');
            }
        }
    }

    //Rotating the martix
    public void rotate() {
        //TODO check if the matrix was correctly initialized
        if (x != y) return;
        char[][] sourceArray = matrix.clone();
        char[][] rotatedArray = new char[sourceArray[0].length][sourceArray.length];
        int positionFactor = rotatedArray.length - 1;
        for (int i = 0; i < rotatedArray.length; i++) {
            for (int j = 0; j < rotatedArray[i].length; j++) {
                int a = i - positionFactor;
                a = (a < 0) ? -a : a;
                rotatedArray[i][j] = sourceArray[j][a];
            }
        }
        matrix = rotatedArray;
    }

    //Method to print
    public void print() {
        for (int i = 0; i < x; i++){
            for (int j = 0; j < y; j++){
                System.out.print(matrix[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println();
    }


    public void convertToSnail() {
        //TODO check if the matrix was correctly initialized
        snail = "";

        int m = matrix.length;
        int n = matrix[0].length;

        int a=0;
        int b=0;

        while(m>0 && n>0){

            if(m==1){
                for(int i=0; i<n; i++){
                    snail = snail + matrix[a][b++] + ",";
                }
                break;
            }else if(n==1){
                for(int i=0; i<m; i++){
                    snail = snail + matrix[a++][b] + ",";
                }
                break;
            }

            for(int i=0;i<n-1;i++){
                snail = snail + matrix[a][b++] + ",";
            }

            for(int i=0;i<m-1;i++){
                snail = snail + matrix[a++][b] + ",";
            }

            for(int i=0;i<n-1;i++){
                snail = snail + matrix[a][b--] + ",";
            }

            for(int i=0;i<m-1;i++){
                snail = snail + matrix[a--][b] + ",";
            }

            a++;
            b++;
            m=m-2;
            n=n-2;
        }
        snail = snail.substring(0,snail.length()-1);
    }
}
