/**
 * Created by tom on 30/08/16.
 */
public class HomeWork {

    public static void main(String[] args) {
        MatrixOps matrix1 = new MatrixOps(3,3);
        //Generate randon matrix
        matrix1.init();
        System.out.println("Initial matrix:");
        //Print the object's matrix to console
        matrix1.print();
        //Rotate the martix and print it again
        matrix1.rotate();
        matrix1.print();

        MatrixOps matrix2 = new MatrixOps(3,5);
        //Generate randon matrix
        matrix2.init();
        System.out.println("Initial matrix:");
        matrix2.print();
        System.out.println("Print snail matrix:");
        //Convert the object's matrix to String, print it after
        matrix2.convertToSnail();
        System.out.println(matrix2.getSnail());
    }
}
